package com.example.examenunidadi;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;


public class RectanguloActivity extends AppCompatActivity {
    private EditText txtBase, txtAltura, txtArea, txtPerimetro;
    private TextView lblNombre;
    private Button btnCalcular, btnRegresar, btnLimpiar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_rectangulo);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valiEmpty()){
                    Toast.makeText(getApplicationContext(),"Por favor no deje espacios vacios", Toast.LENGTH_SHORT).show();
                }
                else{
                    float altura = Float.parseFloat(txtAltura.getText().toString());
                    float base = Float.parseFloat(txtBase.getText().toString());
                    txtArea.setText(String.valueOf(base*altura));
                    txtPerimetro.setText(String.valueOf((altura*2)+(base*2)));

                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPerimetro.setText("");
                txtArea.setText("");
                txtBase.setText("");
                txtAltura.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public boolean valiEmpty(){
        if(txtBase.getText().toString().isEmpty() ||
        txtAltura.getText().toString().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }

    public void iniciarComponentes(){
        lblNombre = (TextView) findViewById(R.id.lblNombre);

        txtAltura = findViewById(R.id.txtAltura);
        txtBase = findViewById(R.id.txtBase);
        txtArea = findViewById(R.id.txtArea);
        txtPerimetro = findViewById(R.id.txtPerimetro);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        Bundle datos = getIntent().getExtras();

        lblNombre.setText(datos.getString("nombre"));

    }
}